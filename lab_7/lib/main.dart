import 'package:flutter/material.dart';

 void main() => runApp(MyApp());


class MyApp extends StatefulWidget {
  @override
  _FormJadwalState createState() => _FormJadwalState();
}

class _FormJadwalState extends State<MyApp> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Form Jadwal"),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              // TextField(),
               Padding(
                padding: EdgeInsets.only(top: 25.0),
                child: new TextFormField(
                  decoration: new InputDecoration(
                    labelText: "Nama Jadwal",
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Nama Jadwal tidak boleh kosong';
                    }
                    return null;
                  },
                  onSaved: (value) {
                    print("title: " + value!);
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 25.0),
                child: new TextFormField(
                  decoration: new InputDecoration(
                    labelText: "Deskripsi",
                    border: OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  onSaved: (value) {
                    print("desc: " + value!);
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 25.0),
                child: new RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _formKey.currentState!.save();
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}