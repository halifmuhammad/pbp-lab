###  Apakah perbedaan antara JSON dan XML?
JSON dan XML sama-sama digunakan untuk mentransfer data. Perbedaannya JSON menggunakan notasi bahasa pemrograman Javascript dan dapat dipandang sebagai object sedankan XML merupakan markup language yang menggunakan tag untuk menyimpan datanya.

<br>

### Apakah perbedaan antara HTML dan XML?
HTML dan XML merupakan markup language. Perbedaan yang mencolok adalah HTML meggunakan tag yag sudah terdefinisi sedangkan tag pada XML bisa disesuaikan sesuai dengan kebutuhan. Selain itu, HTML biasanya digunakan untuk menampilkan data sedangkan XML biasanya digunakan untuk mentransfer data.