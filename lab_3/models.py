from django.db import models
from django.core.exceptions import ValidationError
  
# creating a validator function
def validate_npm(value):
    if len(value) == 10 and value.isdigit():
        return value
    else:
        raise ValidationError("NPM harus 10 digit")

class Friend(models.Model):
    name = models.CharField(max_length = 30)
    # TODO Implement missing attributes in Friend model
    npm = models.CharField(max_length = 10, validators = [validate_npm])
    dob = models.DateField()
