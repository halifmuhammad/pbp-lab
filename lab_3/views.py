from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from .models import Friend
from .forms import FriendForm


@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    failed = False
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3')
        else:
            failed = True
    
    form = FriendForm()
    return render(request, 'lab3_form.html', {'form':form, 'failed':failed})
