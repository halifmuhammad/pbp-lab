from django.shortcuts import render

# Create your views here.
from .models import Note
from .forms import NoteForm
from django.http import HttpResponseRedirect

def index(request):
    notes = Note.objects.all()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    failed = False
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-4')
        else:
            failed = True
    
    form = NoteForm()
    return render(request, 'lab4_form.html', {'form':form, 'failed':failed})

def note_list(request):
    notes = Note.objects.all()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)