import 'package:flutter/material.dart';

import './models/jadwal.dart';

const DUMMY_CATEGORIES = const [
  Jadwal(
    id: '1',
    title: 'Kalkulus',
    kelas: '10',
  ),
  Jadwal(
    id: '2',
    title: 'Trigonometri',
    kelas: '10',
  ),
  Jadwal(
    id: '3',
    title: 'Kalkulus B',
    kelas: '11',
  ),
  
];

