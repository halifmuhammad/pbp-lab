import 'package:flutter/material.dart';

class Jadwal {
  final String id;
  final String title;
  final String kelas;

  const Jadwal({
    required this.id,
    required this.title,
    required this.kelas,
  });
}
