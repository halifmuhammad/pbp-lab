import 'package:flutter/material.dart';

import './dummy_data.dart';
import './screens/jadwal_screen.dart';
import './models/jadwal.dart';

 void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Jadwal',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        accentColor: Colors.indigo,
        canvasColor: Color(0xff171941),
        fontFamily: 'Raleway',
        
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Jadwal'),
        ),
        body: JadwalScreen(),
      ),
      initialRoute: '/', // default is '/'
      routes: {
      //  '/': (ctx) => JadwalScreen(),
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
      },
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (ctx) => JadwalScreen(),
        );
      },
    );
  }
}
