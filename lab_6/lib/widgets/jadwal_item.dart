import 'package:flutter/material.dart';


class JadwalItem extends StatelessWidget {
  final String id;
  final String title;
  final String kelas;

  JadwalItem(this.id, this.title, this.kelas);

  // void selectCategory(BuildContext ctx) {
  //   Navigator.of(ctx).pushNamed(
  //     CategoryMealsScreen.routeName,
  //     arguments: {
  //       'id': id,
  //       'title': title,
  //     },
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
      // onTap: () => selectCategory(context),
    //  splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: Container(
        padding: const EdgeInsets.all(15),
        child: Text(
          title,
          style: Theme.of(context).textTheme.headline6!.copyWith(color: Color(0xffd2d3dc)),
        ),
        decoration: BoxDecoration(
          color: Color(0xff1f2251),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      onTap: () => print(title),
    )
    )
    ;
  }
}
